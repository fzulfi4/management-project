<?php

return array (
  'title' => 'Penginstal Laravel',
  'next' => 'Langkah berikutnya',
  'finish' => 'Install',
  'welcome' => 
  array (
    'title' => 'Selamat Datang Di Penginstal',
    'message' => 'Selamat datang di wizard penyiapan.',
  ),
  'requirements' => 
  array (
    'title' => 'Persyaratan Server',
  ),
  'permissions' => 
  array (
    'title' => 'Izin',
  ),
  'environment' => 
  array (
    'title' => 'Konfigurasi Basis Data',
    'save' => 'Simpan .env',
    'success' => 'Pengaturan file .env Anda telah disimpan.',
    'errors' => 'Tidak dapat menyimpan file .env, Harap buat secara manual.',
  ),
  'install' => 'Install',
  'final' => 
  array (
    'title' => 'Selesai',
    'finished' => 'Aplikasi telah berhasil diinstal.',
    'exit' => 'Klik di sini untuk keluar',
  ),
  'checkPermissionAgain' => 'Periksa Izin Lagi',
);
