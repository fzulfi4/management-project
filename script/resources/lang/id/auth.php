<?php

return array (
  'failed' => 'Kredensial ini tidak cocok dengan catatan kami.',
  'throttle' => 'Terlalu banyak upaya login. Silakan coba lagi dalam :seconds detik.',
  'recaptchaFailed' => 'Recaptcha tidak divalidasi.',
  'sociaLoginFail' => 'Akun Anda tidak ada. Silahkan mendaftar',
  'signInGoogle' => 'Masuk dengan Google',
  'signInFacebook' => 'Masuk dengan Facebook',
  'signInLinkedin' => 'Masuk dengan LinkedIn',
  'signInTwitter' => 'Masuk dengan Twitter',
  'useEmail' => 'atau, gunakan alamat email saya',
  'email' => 'Alamat email',
  'next' => 'Lanjut',
);
