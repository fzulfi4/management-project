<?php

return array (
  'newEvent' => 
  array (
    'subject' => 'Acara Baru Dibuat',
    'text' => 'Acara baru telah dibuat. Unduh lampiran untuk menambahkan acara ke kalender Anda.',
  ),
  'loginDashboard' => 'Buka Dasbor',
  'viewInvoice' => 'Lihat Faktur',
  'thankyouNote' => 'Terima kasih telah menggunakan aplikasi kami!',
  'notificationAction' => 'Tindakan Pemberitahuan',
  'notificationIntro' => 'Pengenalan pemberitahuan.',
  'hello' => 'Halo',
  'whoops' => 'Ups',
  'regards' => 'Salam',
  'newExpense' => 
  array (
    'subject' => 'Biaya baru diajukan',
  ),
  'newExpenseRecurring' => 
  array (
    'subject' => 'Biaya Berulang Baru diajukan',
  ),
  'expenseStatus' => 
  array (
    'subject' => 'Status pengeluaran diperbarui',
    'text' => 'Status pengeluaran Anda diperbarui ke',
  ),
  'expenseRecurringStatus' => 
  array (
    'subject' => 'Status Biaya Berulang diperbarui',
    'text' => 'Status pengeluaran berulang Anda diperbarui ke',
  ),
  'newNotice' => 
  array (
    'subject' => 'Pemberitahuan Baru Diterbitkan',
    'text' => 'Pemberitahuan baru telah diterbitkan. Masuk untuk melihat pemberitahuan.',
  ),
  'newProjectMember' => 
  array (
    'subject' => 'Proyek Baru Ditugaskan',
    'text' => 'Anda telah ditambahkan sebagai anggota proyek',
  ),
  'newProject' => 
  array (
    'subject' => 'Proyek Baru Ditambahkan',
    'text' => 'Sebuah proyek baru ditambahkan dengan nama.',
    'withName' => 'dengan nama.',
    'loginNow' => 'Masuk sekarang untuk melihat proyek.',
  ),
  'newTask' => 
  array (
    'subject' => 'Tugas Baru Ditugaskan untuk Anda',
  ),
  'dueOn' => 'Jatuh tempo pada',
  'newTicket' => 
  array (
    'subject' => 'Tiket Dukungan Baru Diminta',
    'text' => 'Tiket Dukungan Baru diminta. Masuk untuk melihat tiket.',
  ),
  'newUser' => 
  array (
    'subject' => 'Selamat Datang di',
    'text' => 'Akun Anda telah berhasil dibuat.',
  ),
  'leaves' => 
  array (
    'subject' => 'Permintaan cuti baru diterima',
    'statusSubject' => 'Biarkan status aplikasi diperbarui',
  ),
  'taskComplete' => 
  array (
    'subject' => 'Tugas ditandai sebagai selesai',
  ),
  'taskUpdate' => 
  array (
    'subject' => 'Tugas diperbarui',
  ),
  'leave' => 
  array (
    'approve' => 'Biarkan aplikasi disetujui.',
    'reject' => 'Tinggalkan aplikasi ditolak.',
    'applied' => 'Tinggalkan aplikasi diterapkan.',
  ),
  'newClientTask' => 
  array (
    'subject' => 'Tugas Baru Dihasilkan',
  ),
  'reminder' => 
  array (
    'subject' => 'Pengingat untuk tugas yang diberikan',
  ),
  'invoices' => 
  array (
    'paymentReceived' => 'Pembayaran diterima.',
    'paymentReceivedForInvoice' => 'Pembayaran diterima untuk faktur.',
    'paymentReceivedForOrder' => 'Pembayaran diterima untuk pesanan.',
  ),
  'eventReminder' => 
  array (
    'subject' => 'Pengingat Acara',
    'text' => 'Ini untuk mengingatkan Anda tentang acara berikut. Masuk untuk detail lebih lanjut tentang acara tersebut.',
  ),
  'taskReminder' => 
  array (
    'subject' => 'Pengingat Tugas',
  ),
  'estimate' => 
  array (
    'subject' => 'Perkiraan Baru Dibuat',
    'text' => 'Perkiraan baru telah Dibuat. Silakan klik tautan di bawah untuk melihat perkiraan.',
    'loginDashboard' => 'Setujui / Tolak',
  ),
  'invoice' => 
  array (
    'subject' => 'Faktur Baru Diterima',
    'text' => 'Faktur baru telah diterima. Silahkan klik link di bawah ini untuk melihat invoice.',
  ),
  'projectReminder' => 
  array (
    'text' => 'Ini untuk mengingatkan Anda tentang tanggal jatuh tempo proyek-proyek berikut yaitu',
    'subject' => 'Pengingat Proyek',
  ),
  'messages' => 
  array (
    'loginForMoreDetails' => 'Masuk untuk lebih jelasnya.',
  ),
  'taskComment' => 
  array (
    'subject' => 'Komentar baru pada tugas',
  ),
  'taskNote' => 
  array (
    'subject' => 'Catatan baru ditambahkan untuk tugas',
  ),
  'removalRequestAdmin' => 
  array (
    'subject' => 'Permintaan Penghapusan Baru',
    'text' => 'Permintaan penghapusan baru telah diterima',
  ),
  'removalRequestApprovedUser' => 
  array (
    'subject' => 'Persetujuan Permintaan Penghapusan',
    'text' => 'Permintaan penghapusan Anda telah disetujui',
  ),
  'removalRequestRejectedUser' => 
  array (
    'subject' => 'Permintaan Penghapusan Ditolak',
    'text' => 'Permintaan penghapusan Anda telah ditolak',
  ),
  'removalRequestApprovedLead' => 
  array (
    'subject' => 'Persetujuan Permintaan Penghapusan',
    'text' => 'Permintaan penghapusan Anda untuk prospek telah disetujui',
  ),
  'removalRequestRejectedLead' => 
  array (
    'subject' => 'Permintaan Penghapusan Ditolak',
    'text' => 'Permintaan penghapusan Anda untuk prospek telah ditolak',
  ),
  'paymentReminder' => 
  array (
    'subject' => 'Pengingat pembayaran',
    'content' => 'Ini untuk mengingatkan Anda tentang tanggal jatuh tempo pembayaran faktur proyek berikut yaitu:',
  ),
  'fileUpload' => 
  array (
    'subject' => 'File baru diunggah ke proyek:',
  ),
  'payment' => 
  array (
    'subject' => 'Pembayaran diterima!',
    'text' => 'Pembayaran baru telah dilakukan. Login sekarang untuk melihat Pembayaran.',
  ),
  'ticketAgent' => 
  array (
    'subject' => 'Anda telah diberi tiket',
    'text' => 'Anda telah diberi tiket baru.',
  ),
  'subTaskComplete' => 
  array (
    'subject' => 'Sub Tugas ditandai sebagai selesai',
  ),
  'leadAgent' => 
  array (
    'subject' => 'Prospek baru diterima.',
  ),
  'subTaskCreated' => 'Sub tugas dibuat.',
  'discussionReply' => 
  array (
    'subject' => 'balas ke',
    'text' => 'Anda telah menerima balasan di',
  ),
  'discussion' => 
  array (
    'subject' => 'Diskusi baru dimulai',
  ),
  'ticketReply' => 
  array (
    'subject' => 'Balasan tiket baru diterima',
    'text' => 'Anda telah menerima balasan di Tiket#',
  ),
  'contractSign' => 
  array (
    'subject' => 'Kontrak telah ditandatangani',
    'text' => ':contract (kontrak) ditandatangani oleh :client.',
  ),
  'newContract' => 
  array (
    'subject' => 'Kontrak Baru Dibuat!',
    'text' => 'Kontrak baru telah dibuat.',
  ),
  'newChat' => 
  array (
    'subject' => 'Pesan baru diterima.',
  ),
  'noticeUpdate' => 
  array (
    'subject' => 'Pemberitahuan telah diperbarui',
    'text' => 'Pemberitahuan telah diperbarui. Masuk untuk melihat pemberitahuan.',
  ),
  'payments' => 
  array (
    'paymentReceived' => 'Pembayaran diterima',
  ),
  'estimateDeclined' => 
  array (
    'subject' => 'Perkiraan Ditolak',
    'text' => 'Perkiraan telah Ditolak. Silakan klik tautan di bawah ini untuk memeriksa perkiraannya.',
    'loginDashboard' => 'Lihat Perkiraan',
  ),
  'newMeeting' => 
  array (
    'subject' => 'Rapat Baru Dibuat',
    'text' => 'Rapat baru telah dibuat. Unduh lampiran untuk menambahkan rapat ke kalender Anda.',
  ),
  'removalRequestApproved' => 
  array (
    'subject' => 'Persetujuan Permintaan Penghapusan',
    'text' => 'Permintaan penghapusan Anda untuk prospek telah Disetujui',
  ),
  'removalRequestReject' => 
  array (
    'subject' => 'Permintaan Penghapusan Ditolak',
    'text' => 'Permintaan penghapusan Anda untuk prospek telah ditolak',
  ),
  'newTicketRequester' => 
  array (
    'subject' => 'Tiket Baru Telah Dibuat Untuk Anda.',
    'text' => 'Tiket dukungan baru telah dibuat untuk Anda. Masuk untuk melihat tiket.',
  ),
  'newInvoiceRecurring' => 
  array (
    'subject' => 'Faktur Berulang baru dikirimkan',
  ),
  'invoiceRecurringStatus' => 
  array (
    'subject' => 'Status Faktur Berulang diperbarui',
    'text' => 'Status faktur berulang Anda diperbarui ke',
  ),
  'creditNote' => 
  array (
    'subject' => 'Nota Kredit baru diterima',
    'text' => 'Nota kredit baru telah diterima. Login sekarang untuk melihat nota kredit.',
  ),
  'proposal' => 
  array (
    'subject' => 'Proposal Baru Diterima',
    'text' => 'Sebuah proposal baru telah diterima. Login sekarang untuk melihat proposal.',
  ),
  'proposalSigned' => 
  array (
    'subject' => 'Usulan telah diterima!',
    'text' => 'Proposal baru telah diterima dan ditandatangani.',
    'approve' => 'Disetujui oleh',
  ),
  'proposalRejected' => 
  array (
    'subject' => 'Proposal telah ditolak oleh lead!',
    'text' => 'Proposal baru telah ditolak.',
    'rejected' => 'Ditolak',
  ),
  'testMail' => 
  array (
    'testMail' => 'Surat Tes',
    'mailAddress' => 'Masukkan alamat email tempat surat uji harus dikirim',
  ),
  'invitation' => 
  array (
    'subject' => 'telah mengundang Anda untuk bergabung',
    'action' => 'Menerima undangan',
  ),
  'invoiceReminder' => 
  array (
    'subject' => 'Pengingat Faktur',
    'text' => 'Ini untuk mengingatkan Anda tentang tanggal jatuh tempo faktur berikut yaitu',
  ),
  'test' => 
  array (
    'subject' => 'Uji email!',
    'text' => 'Ini adalah email percobaan.',
    'thankyouNote' => 'Terima kasih telah menggunakan aplikasi kami!',
  ),
  'AttendanceReminder' => 
  array (
    'subject' => 'Pengingat Kehadiran',
    'text' => 'Anda lupa menandai kehadiran Anda hari ini. Mohon masuk untuk menghindari ditandai sebagai tidak hadir.',
  ),
  'twoFactor' => 
  array (
    'line1' => 'Kode otentikasi dua faktor Anda adalah',
    'line2' => 'Kode akan kedaluwarsa dalam 10 menit',
    'line3' => 'Jika Anda belum mencoba masuk, abaikan pesan ini.',
  ),
  'newCustomer' => 
  array (
    'subject' => 'Pendaftaran Pelanggan Baru',
    'text' => 'Pelanggan baru telah terdaftar.',
  ),
  'footer' => 'Jika Anda mengalami masalah saat mengeklik tombol ":actionText", salin dan tempel URL di bawah ini ke browser web Anda:',
);
