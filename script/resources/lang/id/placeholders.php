<?php

return array (
  'knowledgeBase' => 'Perbarui gambar profil Anda',
  'name' => 'misalnya John Doe',
  'email' => 'misalnya johndoe@contoh.com',
  'mobile' => 'misalnya 1234567890',
  'password' => 'Harus memiliki minimal 8 karakter',
  'designation' => 'misalnya Ketua Tim',
  'department' => 'misalnya Sumber daya manusia',
  'date' => 'Pilih Tanggal',
  'address' => 'misalnya 132, Jalanku, Kingston, New York 12401',
  'skills' => 'misalnya komunikasi, ReactJS',
  'project' => 'Tulis nama proyek',
  'price' => 'misalnya 10000',
  'hours' => 'misalnya JAM 10 PAGI',
  'category' => 'Masukkan nama kategori',
  'task' => 'Masukkan judul tugas',
  'label' => 'Masukkan judul label',
  'leaveType' => 'Misalnya. Sakit, Santai',
  'colorPicker' => 'Pilih warna',
  'ticketType' => 'misalnya Mendukung',
  'milestone' => 'Masukkan judul tonggak sejarah',
  'milestoneSummary' => 'Masukkan ringkasan pencapaian',
  'sampleText' => 'misalnya Ini adalah contoh teks',
  'webhook' => 'misalnya contohwebhook123',
  'key' => 'misalnya 0f99088a2f31e70daxxxx',
  'secret' => 'misalnya cbabcd0948aed7dd9xxxx',
  'id' => 'misalnya 1275901',
  'cluster' => 'misalnya ap2',
  'note' => 'Masukkan judul catatan',
  'consent' => 'Masukkan nama persetujuan',
  'consentDescription' => 'Jelaskan secara singkat tujuan persetujuan',
  'title' => 'misalnya Pengelola',
  'invoices' => 
  array (
    'invoicePrefix' => 'Masukkan awalan faktur',
    'estimatePrefix' => 'Masukkan awalan perkiraan',
    'creditNotePrefix' => 'Masukkan awalan nota kredit',
    'gstNumber' => 'Masukkan Nomor Pajak',
    'invoiceTerms' => 'Masukkan istilah faktur',
    'description' => 'Masukkan Deskripsi (opsional)',
    'note' => 'misalnya Terima kasih atas bisnis Anda',
    'terms' => 'Sertakan kebijakan pengembalian dan pembatalan Anda',
  ),
  'currency' => 
  array (
    'currencyName' => 'misalnya Dolar',
    'currencySymbol' => 'misalnya $',
    'currencyCode' => 'misalnya USD',
    'thousandSeparator' => 'misalnya ,',
    'decimalSeparator' => 'misalnya .',
  ),
  'ticket' => 
  array (
    'replyTicket' => 'Masukkan judul template',
    'templateText' => 'Masukkan teks template di sini',
  ),
  'columnName' => 'misalnya Tertunda',
  'message' => 'Tulis pesanmu disini',
  'payments' => 
  array (
    'remark' => 'Masukkan ringkasan pembayaran.',
    'transactionId' => 'Masukkan ID transaksi pembayaran',
    'paymentGateway' => 'misalnya PayPal, Stripe, Transfer Bank, Tunai dll.',
  ),
  'expense' => 
  array (
    'item' => 'misalnya Papan Ketik Nirkabel',
    'vendor' => 'misalnya Teknologi Froiden',
  ),
  'status' => 'misalnya Sedang berlangsung',
  'timelog' => 
  array (
    'memo' => 'misalnya Bekerja pada logo baru',
  ),
  'noticeTitle' => 'misalnya Perayaan tahun baru di kantor.',
  'search' => 'Masukkan kata kunci untuk mencari',
  'attendance' => 
  array (
    'workFrom' => 'misalnya Kantor, Rumah, dll.',
  ),
  'dateRange' => 'Tanggal Mulai Sampai Tanggal Berakhir',
  'emailDomain' => 'misalnya gmail.com',
  'slackWebhook' => 'misalnya https://hooks.slack.com/services/XXXXXXXXXXXXXXXXXXXXX',
  'gdpr' => 
  array (
    'additionDescription' => 'Masukkan Deskripsi Tambahan',
  ),
  'productName' => 'misalnya Web Hosting, Laptop, Notebook, Aplikasi Seluler, dll.',
  'leave' => 
  array (
    'reason' => 'misalnya Merasa tidak enak badan',
  ),
  'company' => 'misalnya Acme Corporation',
  'storageSetting' => 
  array (
    'awsKey' => 'misalnya AKIAXXXXX67FNYNM2P',
    'awsBucket' => 'misalnya tes-ember',
  ),
  'hsnSac' => 'misalnya 995431',
  'hourEstimate' => 'misalnya 500',
  'paymentGateway' => 
  array (
    'sandboxPaypalClientId' => 'misalnya AW-Ydt5KHz2FwhAikHsObpRrpB55qE8MyvUkHbQsFb_6_2Unv3WNBSmBxEqA8N74JzOaFTPBUI-MG4sB',
    'livePaypalClientId' => 'misalnya AW-Ydt5KHz2FwhAikHsObpRrpB55qE8MyvUkHbQsFb_6_2Unv3WNBSmBxEqA8N74JzOaFTPBUI-MG4sB',
    'testStripePublishableKey' => 'misalnya sk_test_XXXXXXXXXXxBXnjBe1d6G5reXbGAc8a1qumQN0doumYbhb2tChV6qTCuFfvQyxEDInYho7jhQoR4MqNBWcafRYPCb00r2jkEBKe',
    'liveStripePublishableKey' => 'misalnya sk_live_51GbndSLM4xBXnjBe1d6G5reXbGAc8a1qumQN0doumYbhb2tChV6qTCuFfvQyxEDInYho7jhQoR4MqNBWcafRYPCbXXXXXXXXX',
    'testRazorpayKey' => 'misalnya rzp_test_znKZOLXXT3XXEX',
    'liveRazorpayKey' => 'misalnya rzp_live_znKZOLn4TXXXXX',
    'paystackKey' => 'misalnya pk_live_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
  ),
  'renterPassword' => 'Silakan masukkan kata sandi Anda',
  'city' => 'misalnya New York, Jaipur, Dubai',
  'location' => 'Masukkan lokasi',
);
