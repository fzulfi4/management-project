<?php

return array (
  'password' => 'Kata sandi harus setidaknya enam karakter dan cocok dengan konfirmasi.',
  'reset' => 'Kata sandi Anda telah disetel ulang!',
  'sent' => 'Kami telah mengirimkan email tautan reset kata sandi Anda!',
  'token' => 'Token pengaturan ulang kata sandi ini tidak valid.',
  'user' => 'Kami tidak dapat menemukan pengguna dengan alamat email tersebut.',
  'throttled' => 'Harap tunggu sebelum mencoba lagi.',
);
